﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DidriksonsForms;

namespace TestMemnon
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            txtResult.Text = MemnonIntegration.Save(getOrder());
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            txtResult.Text = MemnonIntegration.Book(getOrder());
        }

        private MemnonOrder getOrder()
        {
            MemnonOrder mo = new MemnonOrder();

            mo.OrderNo = "124578";
            mo.CustomerNo = "1001";
            mo.Name = "Jobre Invest AB";
            mo.Address1 = "Hattmakaregatan 34";
            mo.Zip = "452 38";
            mo.City = "Strömstad";
            mo.CountyCode = "SE";
            mo.DeliverWayDescription = "DHL PAKET";

            mo.KolliTyp1 = "Packet";
            mo.Antal1 = "1";
            mo.Vikt1 = "1";
            mo.Volym1 = "0.5";

            return mo;
        }

        private void btnSoap_Click(object sender, EventArgs e)
        {
            txtResult.Text = MemnonIntegration.WriteSoap(getOrder());
        }
    }
}
