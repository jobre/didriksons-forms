﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GCS;
using System.Configuration;
using System.Windows.Forms;

namespace DidriksonsForms
{
    public class Order : IDisposable
    {
        private Garp.Application app;
        private Garp.IComponents mComp;
        private bool disposed;
        private GarpGenericDB mOGA, mTA, mOGR, mOGK;
        private Garp.Dataset dsOGR;
        private Garp.Dataset dsOGR2;
        private Garp.IComponent lblOS;
        private Garp.IComponent txtOSFlag;
        private Garp.IComponent txtOSText;
        private Garp.IComponent lblC06;
        private Garp.IComponent txtC06;
        private bool bolReadOnly;

        private string orderSourceFlag;

        public Order()
        {
            try
            {
                app = new Garp.Application();

                mComp = app.Components;

                mOGA = new GarpGenericDB("OGA");
                mTA = new GarpGenericDB("TA");
                mOGR = new GarpGenericDB("OGR");
                mOGK = new GarpGenericDB("OGK");

                bolReadOnly = true;

                createFields(bolReadOnly);
                updateData();

                dsOGR = app.Datasets.Item("ogrMcDataSet");
                dsOGR2 = app.Datasets.Item("ogr2McDataSet");

                app.FieldExit += new Garp.IGarpApplicationEvents_FieldExitEventHandler(app_FieldExit);
                dsOGR.AfterScroll += dsOGR_AfterScroll;
                dsOGR.BeforePost += DsOGR_BeforePost;

            }
            catch (Exception e)
            {
                MessageBox.Show("1: " + e.Message);
            }

        }

       
        public string OrderSourceFlag
        {
            get
            {
                if (this.orderSourceFlag == null)
                {
                    try
                    {
                        this.orderSourceFlag = dsOGR2.Fields.Item("C2C").Value.ToString();
                    }

                    catch
                    {
                        this.orderSourceFlag = "0";
                    }
                }

                return this.orderSourceFlag;
            }

            set
            {
                if (value != this.orderSourceFlag)
                {
                    this.orderSourceFlag = value;

                    try
                    {
                        dsOGR2.Fields.Item("C2C").Value = value;
                    }

                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private string orderSourceText;
        public string OrderSourceText
        {
            get
            {
                if (mTA.find(string.Format("OS{0}", this.OrderSourceFlag)))
                {
                    this.orderSourceText = mTA.getValue("TX1");
                }
                else
                {
                    this.orderSourceText = "Unknown";
                }

                return orderSourceText;
            }
        }

        private int orderSourceColor;
        public int OrderSourceColor
        {
            get
            {
                if (mTA.find(string.Format("OS{0}", this.OrderSourceFlag)))
                {
                    this.orderSourceColor = int.Parse(string.Format("{0}{1}", mTA.getValue("FX1"), mTA.getValue("FX2")));
                    if (this.orderSourceColor == 0)
                    {
                        this.orderSourceColor = -16777201;
                    }
                }
                else
                {
                    this.orderSourceColor = -16777201;
                }

                return orderSourceColor;
            }
        }



        private void createFields(bool ReadOnly)
        {
            try
            {
                // Fält som skall visa "C06" från OGR2
                mComp.BaseComponent = "TabSheet3";
                lblC06 = mComp.AddLabel("labelC06");
                lblC06.Text = "Leveranstid";
                lblC06.Visible = true;
                lblC06.TabOrder = 0;
                lblC06.Top = 125;
                lblC06.Left = 562;
                lblC06.Width = 79;
                lblC06.MaxLength = 30;
                lblC06.Height = 20;
                lblC06.TabStop = false;
                lblC06.Checked = false;
                lblC06.TabVisible = false;
                lblC06.ReadOnly = false;

                txtC06 = mComp.AddEdit("textC06");
                txtC06.Text = "lev.tid";
                txtC06.Visible = true;
                txtC06.TabOrder = 2;
                txtC06.Top = 142;
                txtC06.Left = 562;
                txtC06.Width = 79;
                txtC06.MaxLength = 8;
                txtC06.Height = 20;
                txtC06.TabStop = false;
                txtC06.Checked = false;
                txtC06.TabVisible = false;
                txtC06.ReadOnly = true;

                

                mComp.BaseComponent = "TabSheet4";

                lblOS = mComp.AddLabel("orderSourceLabel");
                lblOS.Text = "Order Source:";
                lblOS.Visible = true;
                lblOS.TabOrder = 0;
                lblOS.Top = 293;
                lblOS.Left = 10;
                lblOS.Width = 139;
                lblOS.MaxLength = 0;
                lblOS.Height = 21;
                lblOS.TabStop = false;
                lblOS.Checked = false;
                lblOS.TabVisible = false;
                lblOS.ReadOnly = false;

                txtOSFlag = mComp.AddEdit("orderSourceFlagEdit");
                txtOSFlag.Text = "";
                txtOSFlag.Visible = true;
                txtOSFlag.TabOrder = 2;
                txtOSFlag.Top = 290;
                txtOSFlag.Left = 100;
                txtOSFlag.Width = 15;
                txtOSFlag.MaxLength = 1;
                txtOSFlag.Height = 21;
                txtOSFlag.TabStop = false;
                txtOSFlag.Checked = false;
                txtOSFlag.TabVisible = false;
                txtOSFlag.ReadOnly = ReadOnly;

                txtOSText = mComp.AddEdit("orderSourceTextEdit");
                txtOSText.Text = "";
                txtOSText.Visible = true;
                txtOSText.TabOrder = 2;
                txtOSText.Top = 290;
                txtOSText.Left = 120;
                txtOSText.Width = 82;
                txtOSText.MaxLength = 30;
                txtOSText.Height = 21;
                txtOSText.TabStop = false;
                txtOSText.Checked = false;
                txtOSText.TabVisible = false;
                txtOSText.ReadOnly = true;
            }

            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        void updateData()
        {
            mComp.Item("orderSourceFlagEdit").Text = this.OrderSourceFlag;
            mComp.Item("orderSourceTextEdit").Text = this.OrderSourceText;
            mComp.Item("orderSourceTextEdit").Color = this.OrderSourceColor;
        }


        //ogrPlf2McEdit
        private void DsOGR_BeforePost()
        {
            //var pickFlag = mComp.Item("ogrPlf2McEdit").Text;
            if (!string.IsNullOrEmpty(mComp.Item("ogrPlf2McEdit")?.Text))
            {
                if (mComp.Item("ogrPlf2McEdit")?.Text.CompareTo("1") == 1)
                {
                    System.Windows.Forms.MessageBox.Show("Order being picked, changes not allowed!!.");
                }
            }
            
        }

        void app_FieldExit()
        {
            switch (mComp.CurrentField)
            {
                case "orderSourceFlagEdit":
                    this.OrderSourceFlag = mComp.Item("orderSourceFlagEdit").Text;
                    updateData();
                    break;

                default:
                    break;
            }

            if (mComp.Item("onrEdit").Text != mComp.Item("onrEdit").Text)
            {
                mComp.Item("onrEdit").Text = mComp.Item("onrEdit").Text;
                this.orderSourceFlag = null;
                updateData();
            }
        }
 
        void dsOGR_AfterScroll()
        {
            if (!this.disposed)
            {
                try
                {
                    mComp.Item("textC06").Text = dsOGR2.Fields.Item("C06").Value?.ToString();

                    if (!string.IsNullOrEmpty(mComp.Item("oradEdit").Text))
                    {
                        if (true)//!row.Equals(""))
                        {
                            addRowText(getCustomertext(), mComp.Item("onrEdit").Text.Trim());
                        }
                    }

                    if (mComp.Item("ogrPlf2McEdit").Text != "1")
                    {
                        mComp.Item("minusBitBtn").Visible = false;
                    }
                    else
                    {
                        mComp.Item("minusBitBtn").Visible = true;
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        private string getCustomertext()
        {
            string text = "";

            text = mComp.Item("mOrderSignalText").Text;

            return text;
        }

        private string removeText(string onr, string rdc)
        {
            GarpGenericDB ogr = new GarpGenericDB("OGR");
            GarpGenericDB ogk = new GarpGenericDB("OGK");

            string row = "";
            try
            {
                if(!ogr.find(onr.PadRight(6) + rdc.PadLeft(3)))
                    ogr.next();

                while (ogr.getValue("ONR").Trim().Equals(onr.Trim()) && ogr.getValue("RDC").Trim().Equals(rdc.Trim()) && !ogr.EOF)
                {
                    ogk.find(GCF.noNULL(onr.PadRight(6) + ogr.getValue("RDC").PadLeft(3)));
                    ogk.next();

                    if(row == "")
                        row = ogr.getValue("RDC");

                    while (ogk.getValue("ONR").Trim().Equals(onr) && ogk.getValue("RDC").Trim().Equals(ogr.getValue("RDC").Trim()) && !ogk.EOF)
                    {
                        if (ogk.getValue("FAF").Equals("A"))
                        {
                            ogk.delete();
                        }
                        ogk.next();
                    }

                    ogr.next();
                }

                ogr.Dispose();
                ogk.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("addRowText: " + e.Message);
                row = "";
                ogr.Dispose();
                ogk.Dispose();
            }
            return row;
        }

        private void addRowText(string text, string onr)
        {
            List<OrderRowText> oldText = new List<OrderRowText>();
            List<OrderRowText> addText = new List<OrderRowText>();

            bool isNewText = true;

            try
            {
                mOGR.find(onr);
                mOGR.next();

                while (mOGR.getValue("ONR").Trim().Equals(onr.Trim()) || mOGR.EOF)
                {

                    //MessageBox.Show("Rad: " + mOGR.getValue("RDC"));

                    isNewText = true;
                    addText.Clear();
                    oldText.Clear();
                    
                    //MessageBox.Show(text);
                    
                    if (!string.IsNullOrEmpty(text))
                    {
                        if (text.Length > 60)
                            text = text.Substring(0, 60);

                        OrderRowText o = new OrderRowText();
                        o.Text = text;
                        o.PLF = "1";
                        o.OBF = "0";
                        o.FSF = "0";
                        o.FAF = "A";
                        o.SQC = "255";
                        addText.Add(o);
                    }
                    else
                        return;

                    mOGK.index = 1;
                    if (!mOGK.find(onr.PadRight(6) + mOGR.getValue("RDC").PadLeft(3) + "  1"))
                        mOGK.next();


                    while (mOGK.getValue("ONR").Trim().Equals(onr.Trim()) && mOGK.getValue("RDC").Trim().Equals(mOGR.getValue("RDC").Trim()) && !mOGK.EOF)
                    {
                        //MessageBox.Show("ONR: " + mOGK.getValue("ONR") + " RDC: " + mOGK.getValue("RDC") + " SQC: " + mOGK.getValue("SQC") + " TEXT: " + mOGK.getValue("TX1"));
                        OrderRowText o = new OrderRowText();

                        o.Text = mOGK.getValue("TX1");
                        o.ONR = mOGK.getValue("ONR");
                        o.RDC = mOGK.getValue("RDC");
                        o.SQC = mOGK.getValue("SQC");
                        o.FAF = mOGK.getValue("FAF");
                        o.FSF = mOGK.getValue("FSF");
                        o.OBF = mOGK.getValue("OBF");
                        o.PLF = mOGK.getValue("PLF");

                        if (o.FAF.Equals("A"))
                        {
                            //MessageBox.Show(o.Text.Replace(" ", "").Trim().PadRight(60).Substring(0, 60));
                            //MessageBox.Show(text.Replace(" ", "").Trim().PadRight(60).Substring(0, 60));
                            if (o.Text.Replace(" ", "").Trim().PadRight(60).Substring(0, 60).Equals(text.Replace(" ", "").Trim().PadRight(60).Substring(0, 60)))
                            {
                                isNewText = false;
                            }
                        }

                        oldText.Add(o);
                        mOGK.next();
                        //MessageBox.Show("After NEXT ONR: " + mOGK.getValue("ONR") + " RDC: " + mOGK.getValue("RDC") + " SQC: " + mOGK.getValue("SQC") + " TEXT: " + mOGK.getValue("TX1"));
                    }

                    if (isNewText)
                    {
                        //MessageBox.Show("Done looping ONR: " + mOGK.getValue("ONR") + " RDC: " + mOGK.getValue("RDC") + " SQC: " + mOGK.getValue("SQC") + " TEXT: " + mOGK.getValue("TX1"));
                        removeText(onr, mOGK.getValue("RDC"));

                        foreach (OrderRowText o in oldText)
                        {
                            if (mOGK.find(mOGR.getValue("ONR").PadRight(6) + mOGK.getValue("RDC").PadLeft(3) + o.SQC.PadLeft(3)))
                            {
                                mOGK.delete();
                                addText.Add(o);
                            }
                        }

                        int sqc = 1;
                        foreach (OrderRowText o in addText)
                        {
                            mOGK.insert();
                            mOGK.setValue("ONR", GCF.noNULL(mOGR.getValue("ONR")));
                            mOGK.setValue("RDC", mOGR.getValue("RDC").PadLeft(3));
                            mOGK.setValue("OSE", "K");
                            mOGK.setValue("SQC", "255");
                            mOGK.setValue("TX1", o.Text);
                            mOGK.setValue("OBF", o.OBF);
                            mOGK.setValue("PLF", o.PLF);
                            mOGK.setValue("FSF", o.FSF);
                            mOGK.setValue("FAF", o.FAF);
                            mOGK.post();
                        }
                    }

                    mOGR.next();
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("addRowText: " + e.Message);
            }
        }

        private void on_changeOrderRow()
        {
            if (!this.disposed)
            {
                try
                {
                    mOGA.Dispose();
                    //mOGK.Dispose();

                    GC.Collect();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                }
                catch (Exception ex)
                {
                    //Logger.loggError(ex, "Error in changeOrderRow", "GCS", "");
                }
            }
        }
        
        ~Order()
        {
            Dispose();
        }

        private bool checkOrgNr(string orgnr)
        {
            if (orgnr != null)
                return !orgnr.Equals("");
            else
                return false;

        }

        public virtual void Dispose()
        {
            if (!this.disposed)
            {
                try
                {
                    mOGA.Dispose();
                    mTA.Dispose();
                    mOGR.Dispose();
                    mOGK.Dispose();

                    try
                    {
                        dsOGR.AfterScroll -= dsOGR_AfterScroll;
                        dsOGR.BeforePost -= DsOGR_BeforePost;
                        app.FieldExit -= app_FieldExit;
                    }
                    catch { }

                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(btnMemnon);
                    try
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(lblOS);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(txtOSFlag);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(txtOSText);
                    }
                    catch{}

                    try
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR2);
                    }
                    catch { }

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(mComp);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    GC.Collect();
                }
                finally
                {
                    this.disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }
       
    }

    public class OrderRowText
    {
        public string ONR { get; set; }
        public string RDC { get; set; }
        public string SQC { get; set; }
        public string Text { get; set; }
        public string FAF { get; set; }
        public string OBF { get; set; }
        public string PLF { get; set; }
        public string FSF { get; set; }
    }
}
